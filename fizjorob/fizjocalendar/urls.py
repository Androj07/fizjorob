from django.conf.urls import url
from . import views

app_name = 'fizjocalendar'
urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^/appointment/$', views.appointment, name='appointment'),
]