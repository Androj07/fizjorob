from django.template import loader, Context
from django.core.mail import send_mail

class EmailSender:
    def sendNewAppointmentEmail(appointment):
        template = loader.get_template('appointments/email.txt')
        context = Context({
            'name' : appointment.name,
            'surname' : appointment.surname,
            'age' : appointment.age,
            'date' : appointment.date,
            'time' : appointment.time,
            'phone' : appointment.phone,
            'description' : appointment.description
        })
        send_mail('Welcome to My Project', template.render(context), 'from@address.com', ['andrzejh07@gmail.com'], fail_silently=False)
