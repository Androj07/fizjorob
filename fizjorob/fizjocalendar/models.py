# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Create your models here.
class Appointment(models.Model):
    name = models.CharField(max_length=40)
    surname = models.CharField(max_length=40)
    phone = models.CharField(max_length=12)
    age = models.IntegerField()
    time = models.TimeField()
    date = models.DateField()
    description = models.CharField(max_length=400)
