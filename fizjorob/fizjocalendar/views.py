# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from .models import Appointment
from .modules.communication import EmailSender
def index(request):
    template = loader.get_template('fizjocalendar/index.html')
    return HttpResponse(template.render({}, request))

def appointment(request):
    newAppointment = Appointment( name = request.POST['name'],
                                  surname=request.POST['surname'],
                                  age = request.POST['age'],
                                  date = request.POST['date'],
                                  time = request.POST['time'],
                                  phone = request.POST['phone'],
                                  description = request.POST['description'])

    EmailSender.sendNewAppointmentEmail(newAppointment)
    appointmentSendTmpl = loader.get_template('fizjocalendar/appointment_send.html')
    return HttpResponse(appointmentSendTmpl.render({},request))
